package com.yaremchuk.model.configurations;

import com.yaremchuk.model.beans.BeanD;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@PropertySource("classpath:my.properties")
public class ConfigBeanD {
    @Value("${beanD.name}")
    private String name;
    @Value("${beanD.value}")
    private int value;
    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Order(1)
    public BeanD getBeanD() {
        return new BeanD(name, value);
    }
}