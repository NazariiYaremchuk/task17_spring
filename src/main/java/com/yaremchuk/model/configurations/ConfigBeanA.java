package com.yaremchuk.model.configurations;

import com.yaremchuk.model.beans.BeanA;
import com.yaremchuk.model.beans.BeanB;
import com.yaremchuk.model.beans.BeanC;
import com.yaremchuk.model.beans.BeanD;
import org.springframework.context.annotation.Bean;

public class ConfigBeanA {

    @Bean(name = "FromBC")
    public BeanA getBeanAFromBC(BeanB beanB, BeanC beanC) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanC.getValue());
        return beanA;
    }

    @Bean(name = "FromBD")
    public BeanA getBeanAFromBD(BeanB beanB, BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }


    @Bean(name = "FromCD")
    public BeanA getBeanAFromCD(BeanC beanC, BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanC.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }
}