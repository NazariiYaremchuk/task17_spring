package com.yaremchuk.model.configurations;

import com.yaremchuk.model.beans.BeanA;
import com.yaremchuk.model.beans.BeanE;
import org.springframework.context.annotation.Bean;

public class ConfigBeanE {
    @Bean("beanEFromA1")
    public BeanE getBeanEFirst(BeanA fromBC) {
        return new BeanE(fromBC.getName(), fromBC.getValue());
    }

    @Bean("beanEFromA2")
    public BeanE getBeanESecond(BeanA fromBD) {         //can use Qualifier annotation
        return new BeanE(fromBD.getName(), fromBD.getValue());
    }

    @Bean("beanEFromA3")
    public BeanE getBeanEThird(BeanA fromCD) {
        return new BeanE(fromCD.getName(), fromCD.getValue());
    }
}