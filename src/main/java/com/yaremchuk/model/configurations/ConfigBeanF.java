package com.yaremchuk.model.configurations;

import com.yaremchuk.model.beans.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

public class ConfigBeanF {
    @Bean
    @Lazy
    public BeanF getBeanF() {
        return new BeanF("VladF", 424);
    }
}