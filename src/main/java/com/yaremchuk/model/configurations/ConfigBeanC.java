package com.yaremchuk.model.configurations;

import com.yaremchuk.model.beans.BeanC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@PropertySource("classpath:my.properties")
public class ConfigBeanC {
    @Value("${beanC.name}")
    private String name;
    @Value("${beanC.value}")
    private int value;
    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Order(3)
    public BeanC getBeanC() {
        return new BeanC(name, value);
    }
}