package com.yaremchuk.model.configurations;

import com.yaremchuk.model.beans.BeanB;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@PropertySource("classpath:my.properties")
public class ConfigBeanB {
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private int value;
    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Order(2)
    public BeanB getBeanB() {
        return new BeanB(name, value);
    }
}
