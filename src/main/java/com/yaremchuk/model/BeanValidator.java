package com.yaremchuk.model;

public interface BeanValidator {
    boolean validate();
}
